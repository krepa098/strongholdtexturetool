package main

import (
	"encoding/binary"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"os"
)

type SeekingReader interface {
	io.Reader
	io.Seeker
}

type Image struct {
	data *image.RGBA
}

func (this *Image) Init(width, height int) {
	this.data = image.NewRGBA(image.Rect(0, 0, width, height))
}

func (this *Image) SetPixel(x, y int, r, g, b byte) {
	this.data.Set(x, y, color.RGBA{R: r, G: g, B: b, A: 255})
}

func (this *Image) Save(path, format string) {
	if this.data == nil {
		return
	}

	file, err := os.Create(path + "." + format)
	defer file.Close()
	if err != nil {
		log.Fatal("Save: unable to open file ", err)
	}
	switch format {
	case "jpg":
		jpeg.Encode(file, this.data, nil)
	case "png":
		png.Encode(file, this.data)
	}
}

func (this *Image) DecodeTGX(header TGXHeader, offset, size int64, reader SeekingReader, palette *TGXPalette) {
	this.Init(int(header.Width), int(header.Height))

	var token TGXToken
	var x, y int
	var r, g, b byte
	var pixelData uint16

	reader.Seek(offset, 0)
	read := int64(0)

	for binary.Read(reader, binary.LittleEndian, &token) == nil && read < size {
		switch token.GetType() {
		case TGX_STREAM:
			logVerbose("Token: Stream, Length:", token.GetLength())

			for i := 0; i < token.GetLength(); i++ {
				if palette == nil {
					binary.Read(reader, binary.LittleEndian, &pixelData)
				} else {
					var index byte
					binary.Read(reader, binary.LittleEndian, &index)
					pixelData = palette[set*256+int(index)]
				}
				r, g, b = extractColors(pixelData)
				this.SetPixel(x, y, r, g, b)
				x++
			}

		case TGX_REPEAT:
			logVerbose("Token: Repeat, Length:", token.GetLength())

			if palette == nil {
				binary.Read(reader, binary.LittleEndian, &pixelData)
			} else {
				var index byte
				binary.Read(reader, binary.LittleEndian, &index)
				pixelData = palette[set*256+int(index)]
			}
			r, g, b = extractColors(pixelData)
			for i := 0; i < token.GetLength(); i++ {
				this.SetPixel(x, y, r, g, b)
				x++
			}

		case TGX_TRANSPARENT:
			logVerbose("Token: Transparent, Length:", token.GetLength())
			x += token.GetLength()

		case TGX_NEWLINE:
			logVerbose("Token: New Line")
			y++
			x = 0 //x -= int(header.Width)

		default:
			log.Fatal("ERROR: Unknown token")
		}
		currentPos, _ := reader.Seek(0, 1)
		read = currentPos - offset
	}
}

func (this *Image) DecodeBitmap(header GM1ImageHeader, offset, size int64, reader SeekingReader) {
	reader.Seek(offset, 0)

	this.Init(int(header.Width), int(header.Height)-7)
	rawPixels := make([]uint16, size)
	binary.Read(reader, binary.LittleEndian, &rawPixels)
	//put pixels into image
	var x, y int
	for _, u := range rawPixels {
		r, g, b := extractColors(u)
		this.SetPixel(x, y, r, g, b)
		x++
		if x >= int(header.Width) {
			x = 0
			y++
		}
	}
}

func (this *Image) DecodeTile(offset int64, reader SeekingReader) {
	reader.Seek(offset, 0)

	this.Init(GM1TILEWIDTH, GM1TILEHEIGHT)
	rawPixels := make([]uint16, GM1TILEBYTES/2)
	binary.Read(reader, binary.LittleEndian, &rawPixels)
	//put pixels into image
	var x, y int
	for _, u := range rawPixels {
		r, g, b := extractColors(u)
		this.SetPixel(15-GM1TilePixelsPerLine[y]/2+x, y, r, g, b) //diamond pattern
		x++
		if x >= GM1TilePixelsPerLine[y] {
			x = 0
			y++
		}
	}
}
