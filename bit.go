package main

func bitTest(x int, pos uint) bool {
	return (x & (1 << pos)) > 0
}

func extractBitsetValue(data int, start, count int) (r int) {
	exp := uint(0)
	for i := start; i < start+count; i++ {
		//test bit
		if bitTest(data, uint(i)) {
			r |= 1 << exp
		}
		exp++
	}
	return
}
