package main

import "os"
import "encoding/binary"
import "log"
import "path"

//Color
const (
	TGX_COLOR_BITS     = 5 //0-31
	TGX_RED_START      = 10
	TGX_GREEN_START    = 5
	TGX_BLUE_START     = 0
	TGX_PALETTE_COLORS = 2560 //animated tgx (10 sets of 256 colors[2bytes])
)

//Token
const (
	TGX_LENGTH_BITS = 5
	TGX_NEWLINE     = 7
	TGX_REPEAT      = 6
	TGX_TRANSPARENT = 5
	TGX_STREAM      = 0
)

type TGXToken byte

type TGXHeader struct {
	Width  uint16
	u0     uint16
	Height uint16
	u1     uint16
}

type TGXPalette [TGX_PALETTE_COLORS]uint16 //5120 bytes, 10 sets of 256 16bit colors (always written but only used in animations)

func (this *TGXHeader) Read(reader *os.File) {
	binary.Read(reader, binary.LittleEndian, &this.Width)
	binary.Read(reader, binary.LittleEndian, &this.u0)
	binary.Read(reader, binary.LittleEndian, &this.Height)
	binary.Read(reader, binary.LittleEndian, &this.u1)
}

func extractColors(data uint16) (r, g, b byte) {
	r = byte(extractBitsetValue(int(data), TGX_RED_START, TGX_COLOR_BITS)) * 8
	g = byte(extractBitsetValue(int(data), TGX_GREEN_START, TGX_COLOR_BITS)) * 8
	b = byte(extractBitsetValue(int(data), TGX_BLUE_START, TGX_COLOR_BITS)) * 8
	return
}

func (this *TGXPalette) Read(reader *os.File) {
	binary.Read(reader, binary.LittleEndian, this)
}

func (this TGXToken) GetType() int {
	if bitTest(int(this), uint(TGX_NEWLINE)) {
		return TGX_NEWLINE
	} else if bitTest(int(this), uint(TGX_REPEAT)) {
		return TGX_REPEAT
	} else if bitTest(int(this), uint(TGX_TRANSPARENT)) {
		return TGX_TRANSPARENT
	} else {
		return TGX_STREAM
	}
	return 0
}

func (this TGXToken) GetLength() int {
	return extractBitsetValue(int(this), 0, 5) + 1
}

type TGXImage struct {
	filename     string
	offset, size int
	palette      []uint16
	image        Image
	header       TGXHeader
}

func (this *TGXImage) Load(filename string) {
	this.filename = filename
	
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal("Load: cannot open file", err)
	}

	//Read header 8bytes
	this.header.Read(file)

	log.Println("TGX: Width:", this.header.Width, " Height:", this.header.Height)

	offset, _ := file.Seek(0, 1)
	fileSize, _ := file.Seek(0, 2)
	this.image.DecodeTGX(this.header, offset, fileSize-offset, file, nil)

	
}

func (this *TGXImage) Save() {
	_, filename := path.Split(this.filename)
	savePath := "output/" + filename
	this.image.Save(savePath, "png")
}

func (this *TGXImage) LoadFromMemory(file *os.File, path string, width, height uint16, offset, size int64) {
	this.header.Width = width
	this.header.Height = height

	this.image.DecodeTGX(this.header, offset, size, file, nil)

	this.image.Save(path, "png")
}
